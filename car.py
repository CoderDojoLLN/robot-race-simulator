#!/usr/bin/env python
# -*- coding:utf-8 -*-


import sys, math
import random
import numpy as np
import copy



def distance(pos,size,rayAngle,circuit):
    # Size vehicule in ray direction
    with np.errstate(divide='ignore'):
        sizeCar = np.amin([np.abs(np.divide(size[0]/2.0,np.cos(rayAngle))),np.abs(np.divide(size[1]/2.0,np.sin(rayAngle)))],0)

    # Center circuit around car
    mat = 1.0*circuit
    mat[:,0:2] = np.add(mat[:,0:2],-1.0*pos[0:2])
    mat[:,2:4] = np.add(mat[:,2:4],-1.0*pos[0:2])

    # Rotation ray to be inline with the car
    rayRot = rayAngle+pos[2]

    vecNorm = 1.0*np.arange(2*rayRot.shape[0]).reshape(rayRot.shape[0],2)
    vecNorm[:,0] = np.cos(rayRot)
    vecNorm[:,1] = np.sin(rayRot)

    vecHort = 1.*vecNorm
    vecHort[:,0] = -1.*vecNorm[:,1]
    vecHort[:,1] = vecNorm[:,0]

    v1 = np.dot(mat[:,0:2],np.transpose(vecHort))
    v2 = np.dot(mat[:,2:4],np.transpose(vecHort))
    inRay = np.multiply(v1,v2)<0

    v1 = np.abs(v1)
    v2 = np.abs(v2)

    dist1 = np.multiply(v2,np.dot(mat[:,0:2],np.transpose(vecNorm)))
    dist2 = np.multiply(v1,np.dot(mat[:,2:4],np.transpose(vecNorm)))
    dist = np.divide(np.add(dist1,dist2),np.add(v1,v2))
    dist = np.multiply(dist,inRay)
    dist[(dist<=0)] = 1000

    dist = dist.min(0)
    dist[(dist>100)] = 100
    distCollision = np.add(dist,-1.*sizeCar)

    return( (dist,distCollision,sizeCar) )


class Car_Generic:

    def __init__(self):
        self.model = "generic"
        self.imgNum = 0

        # Car generic definition
        self.size = 1.0*np.array([4.9,2,2.8]) # length, width, wheelbase
        self.position = 1.0*np.array([0,0,math.pi/2.0,0,0]) # x, y, angle, speed, steering

        # # Defining the angle of distance ray
        # # self.rayAngle = 1.0*np.arange(5).reshape(1,5)
        # # self.rayAngle[:,1:] = np.add(np.array([1.0,-1.0,1.0,-1.0])*math.atan(self.size[1]/self.size[0]),np.array([0.0,math.pi,math.pi,2.0*math.pi]))
        # self.rayAngle = np.add(np.array([0,1.0,0,-1.0,0,1.0,0,-1.0])*math.atan(self.size[1]/self.size[0]),np.array([0.0,0.0,math.pi/2,math.pi,math.pi,math.pi,3*math.pi/2,2.0*math.pi]))
        # self.rayAngle.reshape(1,8)
        # print(self.rayAngle)

        # Defining the angle of distance ray
        self.__rays__()

        self.maxSteering = 3.0*math.pi/8.0
        self.mass = 1500.0
        self.seconde100 = 7
        self.power = self.mass*(100/3.6)/self.seconde100  # power = mass * acc
        self.maxSpeed = 55.5
        self.dragCoef = self.power/self.maxSpeed**2

    def __rays__(self):
        self.rayAngle = np.add(np.array([0,1.0,0,-1.0,0,1.0,0,-1.0])*math.atan(self.size[1]/self.size[0]),np.array([0.0,0.0,math.pi/2,math.pi,math.pi,math.pi,3*math.pi/2,2.0*math.pi]))

    def acceleration(self,throttle):
        # throttle in [-100, 100]

        # Acc force
        f = self.power*(throttle/100.0)

        # Stronger brakes
        if f<0:
            f *= 5

        # drag simulation : damper the acceleration with the speed
        f -= self.dragCoef*self.position[3]**2

        # f = m a
        a = f/self.mass

        return(a)


    def move(self,throttle,steeringAngle,dt):
        # throttle [-100,100], steeringAngle [-pi/2,pi/2]

        # limit the throttle
        throttle = min(100,max(-100,throttle))

        # limit the steering and the change of steering per dt
        deltaSteerMult = 1
        steeringAngle = self.position[4]+max(min(steeringAngle-self.position[4],self.maxSteering*dt*deltaSteerMult),-1*self.maxSteering*dt*deltaSteerMult)
        steeringAngle = min(self.maxSteering,max(-1.0*self.maxSteering,steeringAngle))

        # Compute acceleration
        accel = self.acceleration(throttle)
        accel = max(accel,-1*self.position[3]/dt)

        # movement
        dx = self.position[3]*dt+accel*dt**2/2

        # speed
        endposition = self.position*1.0
        endposition[3] = self.position[3]+accel*dt
        endposition[4] = steeringAngle

        if steeringAngle==0:
            #MRUA
            # x, y
            endposition[0] = self.position[0]+math.cos(self.position[2])*dx
            endposition[1] = self.position[1]+math.sin(self.position[2])*dx
            # print("MRU")
            # print(math.cos(self.position[2]))
            # print(math.sin(self.position[2]))

        else:
            # MCUA
            radius = math.tan(math.pi/2.0-(steeringAngle))*self.size[2]
            # print("Radius")
            # print(radius)

            # rotation
            dRot = dx/radius
            # print("dRot")
            # print(dRot)

            # Movement
            posNew = np.array([math.sin(dRot)*radius , (1-math.cos(dRot))*radius])

            # Recenter position
            posNew[0] = posNew[0]+math.cos(dRot)*self.size[2]/2.0
            posNew[1] = posNew[1]+math.sin(dRot)*self.size[2]/2.0

            # Rotation with respect to start
            posNew = np.dot(posNew,np.array([[math.cos(self.position[2]),math.sin(self.position[2])],[-1*math.sin(self.position[2]),math.cos(self.position[2])]]))

            # Translation
            # endposition[0:2] = np.add(posNew,self.position[0:2])
            posNew = np.add(posNew,self.position[0:2])
            posNew[0] = posNew[0]-math.cos(self.position[2])*self.size[2]/2.0
            posNew[1] = posNew[1]-math.sin(self.position[2])*self.size[2]/2.0
            endposition[0:2] = posNew

            # New Angle
            endposition[2] = self.position[2]+dRot

        self.position = endposition


    def distance(self,circuit):
        return( distance(self.position,self.size,self.rayAngle,circuit) )



class car_superb(Car_Generic):
    def __init__(self):
        super().__init__()
        self.model = "Skoda Superb"
        self.imgNum = 0

class car_ferrari(Car_Generic):
    def __init__(self):
        super().__init__()
        self.model = "Ferrari laferrari"
        self.imgNum = 0

        self.size = 1.0*np.array([4.7,1.99,2.65]) # length, width, wheelbase
        self.position = 1.0*np.array([0,0,math.pi/2.0,0,0]) # x, y, angle, speed, steering

        # Defining the angle of distance ray
        self.__rays__()

        self.maxSteering = 3.0*math.pi/8.0
        self.mass = 1370.0
        self.seconde100 = 2.6
        self.power = self.mass*(100/3.6)/self.seconde100  # power = mass * acc
        self.maxSpeed = 270/3.6
        self.dragCoef = self.power/self.maxSpeed**2


class car_twingo(Car_Generic):
    def __init__(self):
        super().__init__()
        self.model = "Renault Twingo"
        self.imgNum = 1

        self.size = 1.0*np.array([3.43,1.8,2.35]) # length, width, wheelbase
        self.position = 1.0*np.array([0,0,math.pi/2.0,0,0]) # x, y, angle, speed, steering

        # Defining the angle of distance ray
        self.__rays__()

        self.maxSteering = 3.0*math.pi/8.0
        self.mass = 790.0
        self.seconde100 = 14.5
        self.power = self.mass*(100/3.6)/self.seconde100  # power = mass * acc
        self.maxSpeed = 73/3.6
        self.dragCoef = self.power/self.maxSpeed**2
