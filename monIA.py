
from ia import *
from game import *


class monIA(IaDegree):
    def __init__(self):
        # Change "myIA" par ton nom ou le nom de ton ia
        self.name = "myIA"

        # Choisit la couleur de ton véhicule
        self.color = np.array([255,0,0])#RGB 0-255

        # Choisit ta voiture
        # self.car = car_twingo() # Twingo un peu cassé, idéal pour apprendre à rouler
        self.car = car_superb() # Break, rapide mais pas trop
        # self.car = car_ferrari() # Ferrari, rapide mais difficile à métriser


    def pilot(self,position,distCollision):
        ########## Entrée ##########

        # position[i] :
        #   - 0 : position en x (gauche - droite)
        #   - 1 : position en y (bas - haut)
        #   - 2 : angle de la voiture (-90 elle va à gauche, 0 vers le haut, 90 vers la droite)
        #   - 3 : vitesse en m/s
        #   - 4 : angle des roues (0 tout droit)
        # Exemple : position[3] donne la vitesse de la voiture

        # distCollision[i] : distance en metre avant collision
        #   - 0 = devant la voiture
        #   - 1 = coin avant gauche
        #   - 2 = gauche
        #   - 3 = coin arrière gauche
        #   - 4 = arrière
        #   - 5 = coin arrière droit
        #   - 6 = droite
        #   - 7 = coin avant droit
        # Exemple : distCollision[0]<20 test si il y a un risque de collision à moins de 20 mètres


        ########## Sortie ##########

        # puisssance : [-100,100]
        #   - -100 : à fond sur les freins
        #   - 100  : à fond sur les gazs

        # volant : [-90 , 90]
        #   - 0 : tout droit
        #   - 90 : touner à fond à gauche
        #   - -90 : tourner à fond à droite


        # Example the code

        # Par défaut on accélère à fond
        puissance = 100.0

        # Si la voiture va à plus de 20m/s
        if position[3]>20:
            # alors on freine à moitié
            puissance = -50


        # Par défaut la voiture va tout droit
        volant = 0

        # Si y a un obstacle devant à moins de 10 mètres
        if distCollision[0]<10:
            # On tourne à gauche un peu à gauche (20°)
            volant = 20

        return( puissance,volant )





# Choisit ton niveau et la version du circuit
level = 9 # 0=facile ... 20=difficile
mission = 3 # 0=aléatoire

# Création du circuit
circuit = getCircuit(mission,level)


# Choisit ton IA
ia = monIA()

# Test ton IA
simulation(ia,circuit,save=True,slowQuit=True)

