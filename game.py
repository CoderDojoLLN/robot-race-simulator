
import sys, math
import random
import numpy as np


from car import *
from ia import *
from circuit import *

# from monIA import *
from geneticBrain import *



def simulation(ia,circuit,save=False,slowQuit=False):

    # Make circuit
    mini,maxi,scale,lenCircuit,circuit,mission,level = circuit

    # Timing simulation
    fps = 30
    dt = 1/fps
    maxT = lenCircuit*10

    # Reset the car position
    car = ia.getCar()
    car.position = 1.0*np.array([0,0,math.pi/2.0,0,0])

    # Save the position history of the car
    positionHistory = np.zeros((maxT+2,8), dtype=float)
    # Save the size of the circuit and the car for visualization
    positionHistory[1,0:2] = mini
    positionHistory[1,2:4] = maxi
    positionHistory[1,4] = scale
    positionHistory[1,5:8] = car.size

    maxSpeed = 0

    t = -1
    score = lenCircuit*maxT+maxT
    win = 0
    while t<maxT-2:
        t += 1

        # Save current time and position
        positionHistory[t+2,5] = t*dt
        positionHistory[t+2,:5] = car.position

        if car.position[3]>maxSpeed:
            maxSpeed = car.position[3]

        # Compute distance collision
        (dist,distCollision,sizeCar) = car.distance(circuit)

        # Update current score
        score = (lenCircuit-car.position[1]+1)*maxT

        # Finished ?
        if car.position[1]>lenCircuit:
            score = t
            win = 1
            break

        # Stop simulation for too slow car
        if (2+car.position[1]) < t/20 and slowQuit:
            break

        # Collision
        if distCollision.min()< 0:
            break

        # Compute the next move of the IA
        pos = np.squeeze(car.position.copy())
        dist = np.squeeze(distCollision.copy())
        throttle,steeringAngle = ia.action(pos,dist)
        positionHistory[t+2,6] = throttle
        positionHistory[t+2,7] = steeringAngle

        # Move the car
        car.move(throttle,steeringAngle,dt)

    # Add the score and the car's color in the positionHistory
    positionHistory[0,0] = score
    positionHistory[0,1:4] = ia.getColor()
    positionHistory[0,4] = ia.getCar().imgNum
    positionHistory[0,5] = t*dt
    positionHistory[0,6] = maxSpeed*3.6

    # Give the silumation outcome to the IA (to learn maybe)
    ia.end(t,score,win)

    # Save position history in CSV
    if save:
        np.savetxt("results/%02d-%02d/%s-%02d-%02d.csv"%(level,mission,ia.getName(),level,mission,), positionHistory[0:(t+2),:], header=ia.getName()+",y,angle,speed,steering,t,throttle,steer", delimiter=",")

    return( positionHistory )





