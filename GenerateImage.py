

import sys, math, os
import random
import numpy as np
from PIL import Image, ImageDraw, ImageFont
import copy




def makeImage(circuit,scale,file,folder):
    circuit = circuit.copy() # to avoid changing circuit
    size = circuit.shape
    mini = (circuit[:,:4]).reshape(size[0]*2,2).min(0,keepdims=True)-1
    maxi = (circuit[:,:4]).reshape(size[0]*2,2).max(0,keepdims=True)+1

    sizeImage = maxi-mini

    circuit[:,0] = circuit[:,0]-mini[:,0]
    circuit[:,2] = circuit[:,2]-mini[:,0]
    circuit[:,1] = maxi[:,1]-circuit[:,1]
    circuit[:,3] = maxi[:,1]-circuit[:,3]
    circuit[:,0:4] = circuit[:,0:4]*scale


    imNew = Image.new("RGB" ,(int(sizeImage[0,0]*scale),int(sizeImage[0,1]*scale)),(255,255,255))
    draw = ImageDraw.Draw(imNew)
    for i in range(size[0]):
        draw.line(tuple(np.array(circuit)[i,0:4]), fill=tuple(circuit[i,4:7].astype(np.int)),width=circuit[i,-1].astype(np.int))

    if not os.path.exists(folder):
        os.mkdir(folder)
    imNew.save(file)#"Circuit.png"

    return( (mini,maxi) )

