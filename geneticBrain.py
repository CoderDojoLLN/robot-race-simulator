import sys, math
import random
import numpy as np

from ia import *
from car import *

class geneticBrainMulti():

    def __init__(self,inputSize,outputSize,popSize,maxi=False):

        self.maxi = maxi

        self.cur = 0
        self.gen = 0
        self.popSize = popSize
        self.perf = np.zeros(self.popSize, dtype=float)

        self.inputSize = inputSize
        self.outputSize = outputSize
        self.randMat = np.random.normal(0, 1, inputSize*outputSize*self.popSize).reshape(inputSize*outputSize,self.popSize)


    def getAction(self,data):
        res = np.zeros(self.outputSize, dtype=float)
        for i in range(self.outputSize):
            resTemp = np.dot(data,self.randMat[i*self.inputSize:(i+1)*self.inputSize,self.cur])
            if -1*resTemp<500:
                res[i] = 1/(1+math.exp(-1*resTemp))
            else:
                res[i] = 0
        return( res )

    def getActionBest(self,data):
        res = np.zeros(self.outputSize, dtype=float)
        for i in range(self.outputSize):
            resTemp = np.dot(data,self.randMat[i*self.inputSize:(i+1)*self.inputSize,0])
            if -1*resTemp<500:
                res[i] = 1/(1+math.exp(-1*resTemp))
            else:
                res[i] = 0
        return( res )


    def endCurr(self,score):
        # Set the score
        if self.perf[self.cur]<0:
            # New subject of the population
            self.perf[self.cur] = score
        self.perf[self.cur] = score#self.perf[self.cur]*0.25+score*0.75
        self.cur += 1

        if self.cur==self.popSize:
            # Update an make a new generation
            self.gen += 1
            print("New generation! %03d - %6.2f"%(self.gen,np.mean(self.perf)))
            randMatNew = np.random.normal(0, 1, self.inputSize*self.outputSize*self.popSize).reshape(self.inputSize*self.outputSize,self.popSize)
            # for i in range(self.inputSize*self.outputSize):
            #     randMatNew[i,:] *= 2*np.std(self.randMat[i,:])
            #     randMatNew[i,:] += np.mean(self.randMat[i,:])

            j = 0
            newPerf = np.zeros(self.popSize, dtype=float)
            # ind = np.where(self.perf <= np.median(self.perf))
            # for i in ind[0]:
            ind = np.argsort(self.perf)[:int(self.popSize/2)]
            for i in ind:
                randMatNew[:,j] = self.randMat[:,i]
                newPerf[j] = self.perf[i]
                j += 1

            # Expo : Higher mean strong selection of the fitest
            expo = 3
            maxPerf = max(newPerf[:j])
            for i in range(j,self.popSize):
                k1 = int(j-np.power(np.random.uniform(0,j**expo),1/expo))
                k2 = int(j-np.power(np.random.uniform(0,j**expo),1/expo))

                propMutation = 0.1
                rand = np.random.uniform(0,1-propMutation,self.inputSize*self.outputSize)
                randMatNew[:,i] = (propMutation*randMatNew[:,i]+rand*randMatNew[:,k1]+(1-propMutation-rand)*randMatNew[:,k2])
                # rand = np.random.uniform(0,1,self.inputSize)
                # randMatNew[:,i] = (rand*randMatNew[:,k1]+(1-rand)*randMatNew[:,k2])

                # rand = np.random.uniform(-1,1,self.inputSize)
                # randMatNew[(rand<0),i] = randMatNew[(rand<0),k1]
                # randMatNew[(rand>=0),i] = randMatNew[(rand>=0),k2]

                # rand = np.random.uniform(0,1,self.inputSize)
                # randMatNew[(rand<propMutation),i] = np.random.normal(0, 1, self.inputSize)[(rand<propMutation)]

                newPerf[i] = -200

            self.perf = newPerf
            self.randMat = randMatNew
            self.cur = j

    def save(self,file):
        np.savetxt("results/%s"%(file,), self.randMat, delimiter=",")

    def load(self,file):
        self.randMat = np.loadtxt("results/%s"%(file,), delimiter=",")


class Ia4(Ia):

    def __init__(self):
        self.name = "Ia4"
        self.color = np.array([random.randint(0,255),random.randint(0,255),random.randint(0,255)])#RGB 0-255

        self.feedbackLoop = 10
        self.previous = np.zeros(self.feedbackLoop, dtype=float)

        self.popSize = 200
        self.brain1 = geneticBrainMulti(16+self.feedbackLoop,40,self.popSize)
        self.brain2 = geneticBrainMulti(40,2+self.feedbackLoop,self.popSize)

        self.best = False

        # Choose your car
        self.car = car_ferrari()
        self.car.rayAngle = np.array([math.pi*i/5-math.pi/2 for i in range(0,11)])

        # Speed Limit
        self.speedLimit = 10
        self.toChangeSpeed = 0

    def save(self):
        self.brain1.save("brain1.csv")
        self.brain2.save("brain2.csv")

    def load(self):
        print('Loading IA ...')
        self.brain1.load("brain1.csv")
        self.brain2.load("brain2.csv")
        print('IA loaded.')

    def getName(self):
        return( "%s_Gen%04d_%03d"%(self.name,self.brain1.gen,self.brain1.cur) )

    def getColor(self):
        self.color = np.array([random.randint(0,255),random.randint(0,255),random.randint(0,255)])#RGB 0-255
        return( self.color )

    def action(self,position,distCollision):
        # Data
        data = np.zeros(16+self.feedbackLoop, dtype=float)+1
        data[2:5] = position[2:]
        data[3] = data[3]/self.car.maxSpeed
        # data[4] = 1
        data[2] = math.pi/2-data[2]
        data[5:16] = distCollision/50
        data[16:] =self.previous

        if self.best:
            resTemp = self.brain1.getActionBest(data)
            res = self.brain2.getActionBest(resTemp)
        else:
            resTemp = self.brain1.getAction(data)
            res = self.brain2.getAction(resTemp)

        throttle = res[0]*200-100
        steeringAngle = res[1]*math.pi/2 - math.pi/4
        self.previous = res[2:]

        # Set acceleration
        if position[3]>self.speedLimit and throttle>0:
             throttle = 0.0

        return( (throttle,steeringAngle) )

    def end(self,t,score,win):
        # Reset previous
        self.previous = np.zeros(self.feedbackLoop, dtype=float)

        if win==1:
            self.toChangeSpeed += 1

        if self.toChangeSpeed>10 and self.brain1.cur%self.popSize==(self.popSize-1):
            self.speedLimit += 5
            self.toChangeSpeed = 0

        if not self.best:
            self.brain1.endCurr(score)
            self.brain2.endCurr(score)
        else:
            print("Score:")
            print(score)






# ia = Ia4()
# ia.load()

# # Simulate
# for i in range(ia.popSize*1000):
#     # print("simu %d"%(i,))
#     simulation(ia,circuit,save=True,slowQuit=True)
#     if i%ia.popSize==(ia.popSize-1):
#         ia.best=True
#         # print("Do best %d"%(i,))
#         simulation(ia,circuit,save=True,slowQuit=False)
#         ia.best=False

#         ia.save()
