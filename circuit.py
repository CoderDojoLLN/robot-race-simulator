
import sys, math
import random
import numpy as np

from GenerateImage import *






def generateCircuit(lenCircuit,stepSize,sd,sd2,nSin,scale,widthCircuit,lenTarget):

    y = [y*stepSize for y in range(int(-2*widthCircuit/stepSize),int(lenCircuit/stepSize)+1,1)]

    rand1 = np.random.normal(0, sd, nSin).reshape(1,nSin)
    rand2 = np.random.normal(0, sd2, nSin)
    x = np.dot(np.sin(np.dot(np.array(y).reshape(len(y),1),rand1)),rand2)
    x = np.multiply(x,np.exp(1.25*(np.array(y)-lenCircuit)/lenTarget))


    mat = np.sqrt(widthCircuit**2-np.subtract.outer(y,y)**2)
    mat1 = np.outer(x,np.array(x)*0.0+1)-mat
    mat2 = np.outer(x,np.array(x)*0.0+1)+mat

    xMin = np.nanmin(mat1,0)
    xMax = np.nanmax(mat2,0)

    circuit = np.zeros((4+(len(y)-1)*2,4), dtype=float)

    circuit[0:(len(y)-1),0] = xMax[:-1]
    circuit[0:(len(y)-1),2] = xMax[1:]
    circuit[0:(len(y)-1),1] = y[:-1]
    circuit[0:(len(y)-1),3] = y[1:]

    circuit[(len(y)-1):2*(len(y)-1),0] = xMin[:-1]
    circuit[(len(y)-1):2*(len(y)-1),2] = xMin[1:]
    circuit[(len(y)-1):2*(len(y)-1),1] = y[:-1]
    circuit[(len(y)-1):2*(len(y)-1),3] = y[1:]

    circuit[-4:,:] = np.array([[min(x)-2*widthCircuit,min(y),max(x)+2*widthCircuit,min(y)],[max(x)+2*widthCircuit,min(y),max(x)+2*widthCircuit,max(y)+3*widthCircuit],[max(x)+2*widthCircuit,max(y)+3*widthCircuit,min(x)-2*widthCircuit,max(y)+3*widthCircuit],[min(x)-2*widthCircuit,max(y)+3*widthCircuit,min(x)-2*widthCircuit,min(y)]])


    nLine = circuit.shape[0]
    circuitDraw = np.zeros((1+nLine+1+int((len(y)-1)/2),8), dtype=float)
    circuitDraw[1:(nLine+1),0:4] = circuit
    circuitDraw[:,7] = scale/5
    circuitDraw[nLine+1,:] = np.array([[min(x)-2*widthCircuit,lenCircuit,max(x)+2*widthCircuit,lenCircuit,255,0,0,scale/10]])

    # Lines
    circuitDraw[nLine+2:,0] = x[range(0,len(y)-1,2)]
    circuitDraw[nLine+2:,2] = x[range(1,len(y),2)]
    circuitDraw[nLine+2:,1] = np.array(y)[range(0,len(y)-1,2)]
    circuitDraw[nLine+2:,3] = np.array(y)[range(1,len(y),2)]

    # circuitDraw[nLine+1:,0:4] = (circuit[range(0,len(y)-1,2),0:4]+circuit[range(len(y)-1,2*(len(y)-1),2),0:4])/2

    return( (circuit,circuitDraw) )



# Generation du circuit
# - mission : numero du cricuit (0 = Aleatoire)
# - level : 0 - 20 , difficulte du circuit
def getCircuit(mission,level):
    # Scale image
    scale = 32.0

    lenCircuit = 500
    stepSize = 0.5
    nSin = 1000

    # Circuit level
    sd = 0.05 # frequence
    if level>10:
        sd *= 2

    # amplitude circuit
    sd2 = 0.2+0.25*(level%5)

    widthCircuit = 4.5*4
    if level>=5:
        widthCircuit /= 2
    if level>=15:
        widthCircuit /= 2

    # First circuit image
    if mission>0:
        np.random.seed(101010+mission)
    print("Circuit generation")
    circuit,circuitDraw = generateCircuit(lenCircuit,stepSize,sd,sd2,nSin,scale,widthCircuit,lenCircuit)
    print("Drawing Circuit")
    mini,maxi = makeImage(circuitDraw,scale,"results/%02d-%02d/Circuit-%02d-%02d.png"%(level,mission,level,mission,),"results/%02d-%02d/"%(level,mission,))

    circuitDraw[0,0:2] = mini
    circuitDraw[0,2:4] = maxi
    circuitDraw[0,4] = scale
    # np.savetxt("csv/circuit.csv", circuitDraw, header="x1,y1,x2,y2,R,G,B,width", delimiter=",")

    return( (mini,maxi,scale,lenCircuit,circuit,mission,level) )




