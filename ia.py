

import sys, math
import random
import numpy as np

from car import *


class Ia:

    def __init__(self):
        self.name = "Generic"
        self.color = np.array([0,0,0])#RGB 0-255

        self.car = car_ferrari()
        # self.car = car_superb()
        # self.car = car_twingo()

    def getCar(self):
        return(self.car)

    def getName(self):
        return( self.name )

    def getColor(self):
        # self.color = np.array([random.randint(0,255),random.randint(0,255),random.randint(0,255)])#RGB 0-255
        return( self.color )

    def action(self,position,distCollision):
        # Dummy IA full throttle
        return( (100,0) )

    def end(self,t,score,win):
        print("Score:")
        print(score)




class IaDegree(Ia):

    def action(self,position,distCollision):
        posNew = position.copy()
        posNew[2] = 180*(posNew[2]-math.pi/2)/math.pi
        posNew[4] = 180*posNew[4]/math.pi
        throttle,steering = self.pilot(posNew,distCollision)
        return( throttle,math.pi*steering/180 )



class IaExample(IaDegree):
    def __init__(self):
        # Change "myIA" par ton nom ou le nom de ton ia
        self.name = "IaExample"

        # Choisit la couleur de ton véhicule
        self.color = np.array([255,0,0])#RGB 0-255

        # Choisit ta voiture
        self.car = car_superb()


    def pilot(self,position,distCollision):
        # Example of code
        throttle = 0.0
        if position[3]<20:
            throttle = 100.0
        if np.min(distCollision[np.array([0, 1,7])])<10*position[3]/30:
            throttle = -100

        # steeringAngle = self.actionHistory[self.t-1,1]
        steeringAngle = position[4]
        if distCollision[0]>10 and min(distCollision[1],distCollision[7])>3:
            # Pas de collision devant
            steeringAngle = -0.2*position[2]
        elif distCollision[1]<distCollision[7]:
            # Droite
            steeringAngle = -90
        else:
            # Gauche
            steeringAngle = 90

        return( throttle,steeringAngle )
